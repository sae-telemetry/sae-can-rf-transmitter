#include <stdint.h>
#include <pthread.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Mailbox.h>
#include "Board.h"

#include "stm32_CAN_SnifferMessage.h"

/* TASK DECLARATIONS*/
extern void *wirelessTxTask(void *arg0);
extern void *uartRxTask(void *arg0);

/* STACK SIZE */
#define THREADSTACKSIZE    1024

/* GLOBAL */
Mailbox_Handle uartMailboxHandler;

int main(void)
{
    pthread_t           wirelessTxHandler,
                        uartHandler;
    pthread_attr_t      wirelessTxAttr,
                        uartHandlerAttr;
    struct sched_param  wirelessTxPriParam,
                        uartPriParam;
    Mailbox_Params      uartMailboxParams;

    CAN_SnifferMessage  canMessage;
    int                 retc;               // return check

    /* BOARD INIT (PWR, CLK, ...)*/
    Board_initGeneral();

    /* MAILBOX CONFIGURATION */
    Mailbox_Params_init(&uartMailboxParams);
    uartMailboxHandler = Mailbox_create(sizeof(canMessage), 10, &uartMailboxParams,NULL);

    /* THREAD ATTRIBUTES INIT */
    pthread_attr_init(&wirelessTxAttr);
    pthread_attr_init(&uartHandlerAttr);

    /* WIRELESS THREAD PARAMETERS SET */
    wirelessTxPriParam.sched_priority = 1;
    retc = pthread_attr_setschedparam(&wirelessTxAttr, &wirelessTxPriParam);
    retc |= pthread_attr_setdetachstate(&wirelessTxAttr, PTHREAD_CREATE_DETACHED);
    retc |= pthread_attr_setstacksize(&wirelessTxAttr, THREADSTACKSIZE);
    if (retc != 0) {
        while (1); // failed to set attributes
    }
    /* UART THREAD PARAMETERS SET */
    uartPriParam.sched_priority = 1;
    retc = pthread_attr_setschedparam(&uartHandlerAttr, &uartPriParam);
    retc |= pthread_attr_setdetachstate(&uartHandlerAttr, PTHREAD_CREATE_DETACHED);
    retc |= pthread_attr_setstacksize(&uartHandlerAttr, THREADSTACKSIZE);
    if (retc != 0)
    {
        while (1); // failed to set attributes
    }

    /* WIRELESS THREAD CREATION */
    retc = pthread_create(&wirelessTxHandler, &wirelessTxAttr, wirelessTxTask, NULL);
    if (retc != 0) {
        while (1);// thread creation error
    }
    /* UART THREAD CREATION */
    retc = pthread_create(&uartHandler, &uartHandlerAttr, uartRxTask, NULL);
    if (retc != 0)
    {
        while (1); // thread creation error
    }
    /* RTOS START */
    BIOS_start();

    return (0);
}
