/* DRIVERS */
#include <ti/drivers/UART.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Mailbox.h>

/* DATA STRUCTURES */
#include "stm32_CAN_SnifferMessage.h"

/* BOARD */
#include "Board.h"

#include <stdbool.h>

/* EXTERNALS */
extern Mailbox_Handle uartMailboxHandler;

static PIN_Handle togglePinHandle;
static PIN_State togglePinState;

PIN_Config togglePinTable[] = {
    CC1310_LAUNCHXL_DIO1 | PIN_INPUT_EN | PIN_PULLDOWN | PIN_IRQ_BOTHEDGES | PIN_HYSTERESIS,          /* Button is active low       */
    PIN_TERMINATE
};

static PIN_Handle ledPinHandle;
static PIN_State ledPinState;

PIN_Config ledPinTable[] = {
    Board_PIN_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,          /* LED is turned off at startup */
    PIN_TERMINATE
};

bool active = false;

void rxToggleComm(PIN_Handle handle, PIN_Id pinId)
{
    (void)handle;
    (void)pinId;

    active = PIN_getInputValue(CC1310_LAUNCHXL_DIO1);
    PIN_setOutputValue(ledPinHandle, Board_PIN_LED0, active);
}

void *uartRxTask(void *arg0)
{
    /* DATA STRUCTURE */
    CAN_SnifferMessage msgReceived;

    togglePinHandle = PIN_open(&togglePinState, togglePinTable);
    if(!togglePinHandle) {
        /* Error initializing button pins */
        while(1);
    }

    /* Setup callback for button pins */
    if (PIN_registerIntCb(togglePinHandle, &rxToggleComm) != 0) {
        /* Error registering button callback function */
        while(1);
    }

    /* Open LED pins */
    ledPinHandle = PIN_open(&ledPinState, ledPinTable);
    if(!ledPinHandle) {
        /* Error initializing board LED pins */
        while(1);
    }

    /* PERIPHERAL DATA STRUCS*/
    UART_Handle uartHandle;
    UART_Params uartParams;

    /* DRIVER INIT */
    UART_init();

    /* SET PERIPHERAL PARAMETERS */
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = 460800;

    // initialize active communication with the current state of the toggle communication signal
    rxToggleComm(NULL, 0);

    while (1)
    {
        if (active)
        {
            /* OPEN CONNECTION */
            uartHandle = UART_open(Board_UART0, &uartParams);
            if (uartHandle == NULL)
            {
                while (1); // UART failed to open
            }
            while (active)
            {
                /* OK ECHO*/
                UART_read(uartHandle, &msgReceived, sizeof(msgReceived));
                Mailbox_post(uartMailboxHandler, &msgReceived, BIOS_WAIT_FOREVER); // Change timeout to drop message
                //UART_write(uartHandle, "OK\r\n", 4);
            }
            UART_close(uartHandle);
        }
    }
}
