/*********************************** RF PACKET DEFINITIONS ****************************************
     -------------------------------------------------------------------------------------------
    |          |           |          |                                                 |       |
    | PREAMBLE | SYNC WORD |  HEADER  |                      PAYLOAD                    |  CRC  |
    |          |           |          |                                                 |       |
     -------------------------------------------------------------------------------------------
                                                                |
                                                                |
                                                                V
                                       ------------------------------------------------
                                      |         |         |                            |
                                      | SEQ NUM | SEQ NUM |    CAN SNIFFER MESSAGE     |
                                      |   0     |    1    |                            |
                                       ------------------------------------------------

**************************************************************************************************/

/************************************************ RX DATA *****************************************
             ----------------------------------------------------------------------------
            |               |                                                 |          |
            |     HEADER    |                      PAYLOAD                    |  STATUS  | -> Status doesnt belong to Packet
            |    [1 byte]   |              [CAN MESSAGE + 2 bytes]            | [1 byte] |    It is added on RX side.
             ----------------------------------------------------------------------------
            |               |                                                 |          |                                       |
            | -> Payload    | -> Seq number                                   |    ?     |
            |    Length     | -> CAN sniffer message                          |    ?     |
            | -> Address    |                                                 |    ?     |
            |    (optional) |                                                 |          |
             ----------------------------------------------------------------------------
**************************************************************************************************/

/*FIXME: Test if rssi info can be shown*/
// Change number of appended bytes if rssi is appended with RF_cmdPropRx.rxConf.bAppendRssi = 1
/* PACKET SIZE DEFINITIONS */
#define NUM_PAYLOAD_EXTRAS      2                           /* Inclusion of 2-byte sequence numbering on TX */
#define MAX_LENGTH              sizeof(CAN_SnifferMessage)\
                                + NUM_PAYLOAD_EXTRAS        /* Max length byte the radio will accept */
#define NUM_DATA_ENTRIES        2                           /* NOTE: Only two data entries supported at the moment */
#define NUM_APPENDED_BYTES      2                           /* The Data Entries data field will contain:
                                                             * 1 Header byte (RF_cmdPropRx.rxConf.bIncludeHdr = 0x1)
                                                             * 1 status byte (RF_cmdPropRx.rxConf.bAppendStatus = 0x1) */
