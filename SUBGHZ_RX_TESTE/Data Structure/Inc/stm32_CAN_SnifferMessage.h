#ifndef __stm32_CAN_SnifferMessage_H
#define __stm32_CAN_SnifferMessage_H
#ifdef STM32F407xx
	#include "stm32f407xx.h"
#endif

#ifdef STM32F469xx
	#include "stm32f469xx.h"
#endif
#include "stdint.h"

enum CAN_Error_Code
{
	ERROR_NONE,
	ERROR_EWG,
	ERROR_EPV,
	ERROR_BOF,
	ERROR_STF,
	ERROR_FOR,
	ERROR_ACK,
	ERROR_BR,
	ERROR_BD,
	ERROR_CRC,
	ERROR_RX_FOV0,
	ERROR_RX_FOV1,
	ERROR_TX_ALST0,
	ERROR_TX_TERR0,
	ERROR_TX_ALST1,
	ERROR_TX_TERR1,
	ERROR_TX_ALST2,
	ERROR_TX_TERR2,
	ERROR_TIMEOUT,
	ERROR_NOT_INITIALIZED,
	ERROR_NOT_READY,
	ERROR_NOT_STARTED,
	ERROR_PARAM,
};

struct CAN_Message_Struct
{
	uint32_t SeqNumber;

	uint8_t Date;
	uint8_t Month;
	uint8_t Year;
	uint8_t Hours;
	uint8_t Minutes;
	uint8_t Seconds;
	uint8_t SubSeconds;

	uint8_t DLC;
	uint32_t canID;
	uint8_t RTR;
	uint8_t IDE;

	// ErrorCode values are represented by CAN_Error_Code enum
	uint16_t ErrorCode;

	uint8_t CAN_Payload[8];

	uint32_t CRC_Value;
};

typedef struct CAN_Message_Struct CAN_SnifferMessage;
#endif /*__stm32_CAN_SnifferMessage_H */
