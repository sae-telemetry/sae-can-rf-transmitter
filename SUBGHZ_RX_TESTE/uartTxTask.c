/* STANDARD LIBRARIES */
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>

/* SYSBIOS */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Mailbox.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

/* Driver configuration */
#include "ti_drivers_config.h"

/* DATA STRUCTURES */
#include "CC13XX_RF_Packet.h"
#include "stm32_CAN_SnifferMessage.h"

#include <stdbool.h>

/* EXTERNALS */
extern Mailbox_Handle wirelessMailboxHandler;

static bool active = false;
uint8_t inputBuffer;

static void uartReadCallback(UART_Handle handle, void* buf, size_t count)
{
    (void)handle;
    (void)count;

    // single char for Xon & Xoff commands
    uint8_t input = *(uint8_t*) buf;
    active = (input == 0x17);

    UART_read(handle, &inputBuffer, sizeof(inputBuffer));
}

void* uartTxTask(void *arg0)
{
    CAN_SnifferMessage msgToSend;
    uint8_t outputBuffer[MAX_LENGTH];  // (MAX LENGTH + APEENDED BYTES) - LENGHT BYTE

    /* PERIPHERAL DATA STRUCS*/
    UART_Handle uartHandle;
    UART_Params uartParams;

    /* DRIVER INIT */
    UART_init();

    /* SET PERIPHERAL PARAMETERS */
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = 460800;
    uartParams.readMode = UART_MODE_CALLBACK;
    uartParams.writeMode = UART_MODE_BLOCKING;
    uartParams.writeTimeout = UART_WAIT_FOREVER;
    uartParams.readCallback = uartReadCallback;

    /* OPEN CONNECTION */
    uartHandle = UART_open(CONFIG_UART_0, &uartParams);
    if (uartHandle == NULL)
    {
        while (1); // UART failed to open
    }

    UART_read(uartHandle, &inputBuffer, sizeof(inputBuffer));
    while (1)
    {
        if(Mailbox_pend(wirelessMailboxHandler, &outputBuffer, 0))
        {
            memcpy(&msgToSend,&outputBuffer[2],sizeof(CAN_SnifferMessage));
            if (active)
            {
                UART_write(uartHandle, &msgToSend, sizeof(msgToSend));
            }
        }
    }
}
