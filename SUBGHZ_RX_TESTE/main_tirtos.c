#include <stdint.h>
#include <pthread.h>

#include <ti/sysbios/knl/Mailbox.h>
#include <ti/sysbios/BIOS.h>

#include "ti_drivers_config.h"

#include "stm32_CAN_SnifferMessage.h"

extern void *wirelessRxTask(void *arg0);
extern void *uartTxTask(void *arg0);

/* GLOBAL */
Mailbox_Handle wirelessMailboxHandler;

/* Stack size in bytes */
#define THREADSTACKSIZE    2096

int main(void)
{
    pthread_t           wirelessRxHandler,
                        uartHandler;
    pthread_attr_t      wirelessRxAttr,
                        uartHandlerAttr;
    struct sched_param  wirelessRxPriParam,
                        uartPriParam;
    Mailbox_Params      wirelessMailboxParams;
    int                 retc;

    /* BOARD INIT (PWR, CLK, ...)*/
    Board_initGeneral();

    /* MAILBOX CONFIGURATION */
    Mailbox_Params_init(&wirelessMailboxParams);
    wirelessMailboxHandler = Mailbox_create(sizeof(CAN_SnifferMessage)+3, 10, &wirelessMailboxParams,NULL);

    /* THREAD ATTRIBUTES INIT */
    pthread_attr_init(&wirelessRxAttr);
    pthread_attr_init(&uartHandlerAttr);

    /* WIRELESS THREAD PARAMETERS SET */
    wirelessRxPriParam.sched_priority = 1;
    retc = pthread_attr_setschedparam(&wirelessRxAttr, &wirelessRxPriParam);
    retc |= pthread_attr_setdetachstate(&wirelessRxAttr, PTHREAD_CREATE_DETACHED);
    retc |= pthread_attr_setstacksize(&wirelessRxAttr, THREADSTACKSIZE);
    if (retc != 0) {
        while (1); //// failed to set attributes
    }
    /* UART THREAD PARAMETERS SET */
    uartPriParam.sched_priority = 1;
    retc = pthread_attr_setschedparam(&uartHandlerAttr, &uartPriParam);
    retc |= pthread_attr_setdetachstate(&uartHandlerAttr, PTHREAD_CREATE_DETACHED);
    retc |= pthread_attr_setstacksize(&uartHandlerAttr, THREADSTACKSIZE);
    if (retc != 0)
    {
        while (1); // failed to set attributes
    }

    /* WIRELESS THREAD CREATION */
    retc = pthread_create(&wirelessRxHandler, &wirelessRxAttr, wirelessRxTask, NULL);
    if (retc != 0)
    {
        while (1);// thread creation error
    }
    /* UART THREAD CREATION */
    retc = pthread_create(&uartHandler, &uartHandlerAttr, uartTxTask, NULL);
    if (retc != 0)
    {
      //  while (1); // thread creation error
    }

    /* RTOS START */
    BIOS_start();

    return (0);
}
